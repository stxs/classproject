using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeMoney : MonoBehaviour
{
    public int inputMoney;
    // Start is called before the first frame update
    void Start()
    {
        print("1000共" + inputMoney / 1000 + "張");
        inputMoney -= inputMoney / 1000 * 1000;
        print("500共" + inputMoney / 500 + "張");
        inputMoney -= inputMoney / 500 * 500;
        print("100共" + inputMoney / 100 + "張");
        inputMoney -= inputMoney / 100 * 100;
        print("50共" + inputMoney / 50 + "個");
        inputMoney -= inputMoney / 50 * 50;
        print("10共" + inputMoney / 10 + "個");
        inputMoney -= inputMoney / 10 * 10;
        print("5共" + inputMoney / 5 + "個");
        inputMoney -= inputMoney / 5 * 5;
        print("1共"+inputMoney+"個");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
