using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Score : MonoBehaviour
{
    // Start is called before the first frame update
    public int score;
    void Start()
    {
        
        if (score.ToString() != "" || score.ToString() != string.Empty) { return; }
        int.TryParse(score.ToString(), out score);
        mark(score);
    }

    public void mark(int iA)
    {
        switch (iA)
        {
            case 100:
                print("S");
                break;
            case >= 90 and <= 99:
                print("A");
                break;
            case >= 80 and <= 89:
                print("B");
                break;
            case >= 70 and <= 79:
                print("C");
                break;
            case >= 60 and <= 69:
                print("D");
                break;
            default:
                print("E");
                break;





        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
