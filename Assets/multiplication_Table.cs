using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class multiplication_Table : MonoBehaviour
{
    // Start is called before the first frame update
    public string mt;
    public Text mt_text;
    void Start()
    {
        for (int i = 1; i < 10; i++)
        {
            for (int j= 1; j<10; j++)
            {
                mt = mt + (i+"*" + j + "=" + j * i+" " );
            }
            mt = mt + "\n";
        }
        mt_text.text = mt;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
