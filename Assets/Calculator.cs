using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Calculator : MonoBehaviour
{

    
    public Button[] Btns;
    public Text ShowN;
    public Button[] Btnsc;
    public string SaveSun;
    public string SaveNum;
    public string SaveSign;
    // Start is called before the first frame update
    void Start()
    {

    }

    public void ShowNum(int iValue)
    {
        //Debug.Log(iValue);
        ShowN.text = "";
        SaveNum += iValue.ToString();
        ShowN.text = SaveNum;

    }
    public void SaveC(string iString)
    {
        SaveSign = iString;
        SaveSun = SaveNum;
        SaveNum = "";
        Debug.Log("SaveC_SaveSun:" + SaveSun);
        Debug.Log("SaveC_SaveNum:" + SaveNum);
        ShowN.text = iString;

    }
    public void ToSum()
    {
        switch (SaveSign)
        {
            case "+":
                Debug.Log(SaveSun);
                Debug.Log(SaveNum);
                SaveSun = (int.Parse(SaveSun) + int.Parse(SaveNum)).ToString();
                Debug.Log(SaveSun);
                Debug.Log(SaveNum);
                break;
            case "-":
                Debug.Log(SaveSun);
                Debug.Log(SaveNum);
                SaveSun = (int.Parse(SaveSun) - int.Parse(SaveNum)).ToString();
                Debug.Log(SaveSun);
                Debug.Log(SaveNum);
                break;
            case "*":
                Debug.Log(SaveSun);
                Debug.Log(SaveNum);
                SaveSun = (int.Parse(SaveSun) * int.Parse(SaveNum)).ToString();
                Debug.Log(SaveSun);
                Debug.Log(SaveNum);
                break;
            case "/":
                Debug.Log(SaveSun);
                Debug.Log(SaveNum);
                SaveSun = (int.Parse(SaveSun) / int.Parse(SaveNum)).ToString();
                Debug.Log(SaveSun);
                Debug.Log(SaveNum);
                break;
            default:
                SaveSun = "";
                return;
        }
        ShowN.text = SaveSun;
        SaveNum = SaveSun;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Keypad0))
        {
            ShowNum(0);
        }

        if (Input.anyKeyDown)
        {
            for (int i =0; i < System.Enum.GetValues(typeof(KeyCode)).Length; i++)
            {
                if (Input.GetKeyDown((KeyCode )i))
                {
                    Debug.LogError((KeyCode)i);
                    //ShowNum(i);
                    switch (i)
                    {
                        case (>= 256 and <= 265) :
                            ShowNum(i - 256);
                            break;
                        case (>= 48 and <= 57):
                            ShowNum(i - 48);
                            break;
                        case 8:
                            int SaveNumL = SaveNum.Length;
                            SaveNum =SaveNum.Substring(0,SaveNumL-1);
                            ShowN.text = SaveNum;
                            break;
                        case 267:
                            SaveC("/");
                            break;
                        case 268:
                            SaveC("*");
                            break;
                        case 269:
                            SaveC("-");
                            break;
                        case 270:
                            SaveC("+");
                            break;
                        case 13 or 271:
                            ToSum();
                            break;


                        default:
                            break;



                    }
                }
            }
        }
    }
    /*
    public Text showText;
    public Text showRPS;
    public int rndN, iRPS;
    public string[] getSign;
    public string getRPS1, getRPS2;

     public void showSign(string IString)
     {
         getRPS1 = IString;
         showText.text = getRPS1;
         rndN = Random.Range(1, 4);
         switch (rndN)
         {
             case 1:
                 getRPS2 = "剪刀";
                 break;
             case 2:
                 getRPS2 = "石頭";
                 break;
             case 3:
                 getRPS2 = "布";
                 break;
         }


         if (getRPS1== "剪刀")
         {
             if(getRPS2 == "剪刀")
             {
                 showText.text = "玩家:"+getRPS1+" 電腦:"+getRPS2+" 平手";
             }else if (getRPS2 == "石頭")
             {
                 showText.text = "玩家:" + getRPS1 + " 電腦:" + getRPS2 + " 電腦贏了";
             }else if (getRPS2 == "布")
             {
                 showText.text = "玩家:" + getRPS1 + " 電腦:" + getRPS2 + " 玩家贏了";
             }
         }

         if (getRPS1 == "石頭")
         {
             if (getRPS2 == "石頭")
             {
                 showText.text = "玩家:" + getRPS1 + " 電腦:" + getRPS2 + " 平手"; ;
             }
             else if (getRPS2 == "布")
             {
                 showText.text = "玩家:" + getRPS1 + " 電腦:" + getRPS2 + " 電腦贏了";
             }
             else if (getRPS2 == "剪刀")
             {
                 showText.text = "玩家:" + getRPS1 + " 電腦:" + getRPS2 + " 玩家贏了";
             }
         }

         if (getRPS1 == "布")
         {
             if (getRPS2 == "布")
             {
                 showText.text = "玩家:" + getRPS1 + " 電腦:" + getRPS2 + " 平手"; 
             }
             else if (getRPS2 == "剪刀")
             {
                 showText.text = "玩家:" + getRPS1 + " 電腦:" + getRPS2 + " 電腦贏了";
             }
             else if (getRPS2 == "石頭")
             {
                 showText.text = "玩家:" + getRPS1 + " 電腦:" + getRPS2 + " 玩家贏了";
             }
         }

     }



    /*
    public void showSign(string IString)
    {
        getRPS1 = IString;
        showText.text = getRPS1;
        rndN = Random.Range(1, 4);
        switch (rndN)
        {
            case 1:
                getRPS2 = "剪刀";
                break;
            case 2:
                getRPS2 = "石頭";
                break;
            case 3:
                getRPS2 = "布";
                break;
        }

        if ((getRPS1== "剪刀" & getRPS2 == "剪刀") | (getRPS1 == "石頭" & getRPS2 == "石頭") | (getRPS1 == "布" & getRPS2 == "布"))
        {
            showText.text = "玩家:" + getRPS1 + " 電腦:" + getRPS2 + " 平手";
        }
        if ((getRPS1 == "剪刀" & getRPS2 == "石頭") | (getRPS1 == "石頭" & getRPS2 == "布") | (getRPS1 == "布" & getRPS2 == "剪刀"))
        {
            showText.text = "玩家:" + getRPS1 + " 電腦:" + getRPS2 + " 電腦贏了";
        }
        if ((getRPS1 == "剪刀" & getRPS2 == "布") | (getRPS1 == "石頭" & getRPS2 == "剪刀") | (getRPS1 == "布" & getRPS2 == "石頭"))
        {
            showText.text = "玩家:" + getRPS1 + " 電腦:" + getRPS2 + " 玩家贏了";
        }
    }
    */

}


