using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    // Start is called before the first frame update
    private Vector3 Dir;
    private float Speed;
    private float Atk;
    

    public void Init(Vector3 iDir,float iSpeed, float iAtk)
    {
        Dir= iDir;
        Speed= iSpeed;
        Atk = iAtk;
    }


    // Update is called once per frame
    void Update()
    {
        transform.position += Dir * Time.deltaTime * Speed;

        if (transform.position.y > 7 || transform.position.y<-7)
        {
            Destroy(this.gameObject); 
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Enemy")
        {
            var aEnemy=collision.GetComponent<Enemy>();
            aEnemy.OnHit(Atk);
            Destroy(this.gameObject);

        }
    }
}
