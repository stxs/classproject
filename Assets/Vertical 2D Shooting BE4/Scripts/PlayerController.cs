using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    // Start is called before the first frame update

    //public Transform transform;
    public Transform ShootPoint;
    public GameObject Bullet;
    public float Speed = 10.5f;
    public Animator anim;
    void Start()
    {
        //MoveArea = new Vector4(2, 5, -2, -5);//�k �W �� �U
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 Dir = new Vector3(0, 0, 0 );
        if (Input.GetKeyDown(KeyCode.Space))
        {
            /*
            var aGo = Instantiate(Resources.Load<GameObject>("PlayerBullet"));
            aGo.transform.position = ShootPoint.position;
            */
            var aBullet = Instantiate(Resources.Load<Bullet>("PlayerBullet"));
            aBullet.transform.position = ShootPoint.position;
            aBullet.Init(new Vector3(0,1, 0), 3,1);
        }

        if (Input.GetKey(KeyCode.W))
        {
            Dir.y = 1;
        }
        if (Input.GetKey(KeyCode.S))
        {
            Dir.y = -1;

        }
        if (Input.GetKey(KeyCode.A))
        {
            Dir.x = -1;
        }
        if (Input.GetKey(KeyCode.D))
        {
            Dir.x = 1;
        }
        //anim.SetInteger("DirX",(int)Dir.x);

        if(Dir.x > 0)
        {
            anim.Play("playright");
        }
        else if(Dir.x < 0){
            anim.Play("playleft");
        }
        else
        {
            anim.Play("playidle");
        }
        

        transform.position += Dir * Time.deltaTime * Speed;
    }
}
